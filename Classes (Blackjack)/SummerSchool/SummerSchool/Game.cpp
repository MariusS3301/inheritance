#include "stdafx.h"
#include "Game.h"
#include "Player.h"
#include "NaturalPolicy.h"

#include <algorithm>

Game::Game(size_t numPlayers, Player ** players) : 
	m_numPlayers(0),
	m_players(nullptr),
	m_dealer()
{
	size_t numTotalPlayers = numPlayers + 1; // add the dealer player
	m_players = new Player*[numTotalPlayers];

	for (size_t idx = 0; idx < numPlayers; ++idx)
	{
		m_players[idx] = players[idx];
	}

	m_players[numPlayers] = new Player("Dealer");
	m_numPlayers = numTotalPlayers;
	m_winPolicy = new NaturalPolicy();
}

Game::~Game()
{
	delete m_players[m_numPlayers - 1];
	delete[] m_players;
	m_players = nullptr;
	delete m_winPolicy;
}

void Game::Play()
{
	if (m_numPlayers < 2) return;

	m_dealer.Shuffle();

	DealInitialCards();

	//test
	PrintPlayers();

	GameLoop();
}

void Game::DealInitialCards()
{
	for (size_t idx = 0; idx < m_numPlayers; ++idx)
	{
		m_dealer.Deal(m_players[idx]);
		m_dealer.Deal(m_players[idx]);
	}
}

void Game::GameLoop()
{
	bool allStand = false;
	bool* playerStands = new bool[m_numPlayers];
	std::fill(playerStands, playerStands + m_numPlayers, false);

	while (!allStand)
	{
		allStand = true;
		for (size_t idx = 0; idx < m_numPlayers; ++idx)
		{
			if (playerStands[idx]) continue;

			Player* player = m_players[idx];
			Action action = player->GetAction();
			if (action == HIT)
			{
				allStand = false;
				m_dealer.Deal(player);
				printf("%s Hits!\n", player->GetName());
			}
			else
			{
				playerStands[idx] = true;
				printf("%s Stands!\n", player->GetName());
			}
		}

		
	}
	for (size_t i = 0; i < m_numPlayers-1; ++i)
	{
		if (DeteminWinner(i))
		{
			m_players[i]->setBet(m_winPolicy->getPrize(m_players[i]->getBet()));
		}
	}
	PrintPlayers();
	delete[] playerStands;
	playerStands = nullptr;
}


void Game::PrintPlayers() const
{
	for (size_t idx = 0; idx < m_numPlayers; ++idx)
	{
		Player* player = m_players[idx];
		printf("Name: %s\n", player->GetName());

		player->GetHand().Print();
		player->printSomething();
		printf("\n");
	}
}

bool Game::DeteminWinner(const int& index) const
{
	return m_players[index] > m_players[m_numPlayers - 1];
}
