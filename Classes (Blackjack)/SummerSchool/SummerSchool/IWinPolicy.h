#pragma once

class IWinPolicy
{
public:
	virtual float getPrize(const float& bet)=0;
};