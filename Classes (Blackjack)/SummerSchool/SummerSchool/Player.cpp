#include "stdafx.h"
#include "Player.h"
#include <iostream>
#include <cstring>

Player::Player(const char * name) : m_name(nullptr), m_hand(),m_bet(10)
{
	m_name = new char[strlen(name) + 1]; // add 1 for '\0'
	strcpy(m_name, name);
}

Player::~Player()
{
	delete[] m_name;
	m_name = nullptr;
}

const char * Player::GetName() const
{
	return m_name;
}

Hand & Player::GetHand()
{
	return m_hand;
}

Action Player::GetAction() const
{
	Action returnValue = STAND;
	if (m_hand.GetValue() < 17) returnValue = HIT;

	return returnValue;
}

float Player::getBet() const
{
	return m_bet;
}

void Player::setBet(const float & bet)
{
	m_bet = bet;
}

void Player::printSomething() const
{
	std::cout << m_name << " " << m_bet;
}


