#pragma once
#include "PileOfCards.h"
class Card;
class Shoe:public PileOfCards
{
public:
	Shoe();
	~Shoe();

	void SetCards(size_t numCards, Card** cards);
	Card* NextCard();

private:
	size_t m_nextCardIndex;
};

