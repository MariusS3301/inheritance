#include "stdafx.h"
#include "Dealer.h"
#include "Card.h"
#include "Player.h"

#include <algorithm>
#include <ctime>
#include <cstdlib>

Dealer::Dealer() :m_deck(), m_shoe()
{
}

Dealer::~Dealer()
{
}

void Dealer::Shuffle()
{
	size_t numCards = m_deck.GetNumberOfCards();
	Card** cards = m_deck.GetCards();
	
	std::random_shuffle(cards, cards + numCards);
	
	m_shoe.SetCards(numCards, cards);
}

void Dealer::Deal(Player * player)
{
	Card* card = m_shoe.NextCard();
	if (card == nullptr) return;

	player->GetHand().AddCard(card);
}
