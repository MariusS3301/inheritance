#pragma once

enum State
{
	NONE,
	LOSE,
	WIN,
	NATURAL,
	PUSH,
};

class Dealer;
class Player;

class PlayerStates
{
public:
	State whatIsTheState(Dealer* dealer, Player * player);
};

