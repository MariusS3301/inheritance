#pragma once
#include "Hand.h"

enum Action
{
	HIT,
	STAND
};

class Player
{
public:
	Player(const char* name);
	~Player();

	const char* GetName() const;
	Hand& GetHand();

	Action GetAction() const;

	float getBet() const;
	void setBet(const float& bet);

	void printSomething()const;

private:
	char* m_name;
	Hand m_hand;
	float m_bet;
};

