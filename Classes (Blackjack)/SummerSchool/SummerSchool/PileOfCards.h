#pragma once
#include "Card.h"
class PileOfCards
{
public:
	PileOfCards();
	~PileOfCards();
protected :
	Card** m_cards;
	size_t m_numCards;
};

