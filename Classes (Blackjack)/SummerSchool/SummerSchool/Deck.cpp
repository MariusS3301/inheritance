#include "stdafx.h"
#include "Deck.h"
#include "Card.h"

Deck::Deck() :PileOfCards()
{
}

Deck::~Deck()
{
}

size_t Deck::GetNumberOfCards() const
{
	return m_numCards;
}

Card ** Deck::GetCards() const
{
	return m_cards;
}
