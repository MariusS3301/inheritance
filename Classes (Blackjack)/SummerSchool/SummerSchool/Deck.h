#pragma once
#include "PileOfCards.h"
class Card;

class Deck:public PileOfCards
{
public:
	Deck();
	~Deck();

	size_t GetNumberOfCards() const;
	Card** GetCards() const;
};

